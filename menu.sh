#!/bin/bash
# Script by: Teletubbins
# Menu Section

echo -e "\e[0m                                                   "
echo -e "\e[94m =========================================================="
echo -e "\e[0m                                                   "
echo -e "\e[94m                  AutoScript by Teletubbins                "
echo -e "\e[0m                                                   "
echo -e "\e[93m            [1]  Create"
echo -e "\e[93m            [2]  Trial"
echo -e "\e[93m            [3]  Remove"
echo -e "\e[93m            [4]  Check"
echo -e "\e[93m            [5]  Member"
echo -e "\e[93m            [6]  Banner"
echo -e "\e[93m            [7]  Restart"
echo -e "\e[93m            [8]  Speedtest"
echo -e "\e[93m            [9]  Info"
echo -e "\e[93m            [10] About"
echo -e "\e[93m            [x]  Exit"
echo -e "\e[0m                                                   "
read -p "                 Select From Options [1-10 or x] :  " Menu
echo -e "\e[0m                                                   "
echo -e "\e[94m ==========================================================\e[0m"

case $Menu in
		1)
		create
		exit
		;;
		2)
		trial
		exit
		;;
		3)
		remove
		exit
		;;
		4)
		check
		exit
		;;
		5)
		member
		exit
		;;
		6)
		banner
		exit
		;;
		7)
		restart
		exit
		;;
		8)
		speedtest
		exit
		;;
		9)
		info
		exit
		;;
		10)
		about
		exit
		;;
	esac