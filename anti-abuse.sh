#!/bin/bash
# Modified by Teletubbins

# Bruteforce Protection
iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --set 
iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --update --seconds 60 --hitcount 10 -j DROP  

# Protection Port Scanning
iptables -N port-scanning 
iptables -A port-scanning -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s --limit-burst 2 -j RETURN 
iptables -A port-scanning -j DROP

# Sony Playstation Blocking
iptables -A FORWARD -m string --algo bm --string "account.sonyentertainmentnetwork.com" -j DROP
iptables -A FORWARD -m string --algo bm --string "auth.np.ac.playstation.net" -j DROP
iptables -A FORWARD -m string --algo bm --string "auth.api.sonyentertainmentnetwork.com" -j DROP
iptables -A FORWARD -m string --algo bm --string "auth.api.np.ac.playstation.net" -j DROP

# Block Torrent
iptables -A OUTPUT -p tcp --dport 6881:6889 -j DROP
iptables -A OUTPUT -p udp --dport 1024:65534 -j DROP
iptables -A FORWARD -m string --string "get_peers" --algo bm -j DROP
iptables -A FORWARD -m string --string "announce_peer" --algo bm -j DROP
iptables -A FORWARD -m string --string "find_node" --algo bm -j DROP
iptables -A FORWARD -m string --algo bm --string "BitTorrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "BitTorrent protocol" -j DROP
iptables -A FORWARD -m string --algo bm --string "peer_id=" -j DROP
iptables -A FORWARD -m string --algo bm --string ".torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce.php?passkey=" -j DROP
iptables -A FORWARD -m string --algo bm --string "torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce" -j DROP
iptables -A FORWARD -m string --algo bm --string "info_hash" -j DROP

rm -f /root/anti-abuse.sh

clear
echo "Anti-Abuse Succesfully Installed!"
echo " "
echo "Features:"
echo "Bruteforce Protection"
echo "Port Scanning Protection"
echo "Sony Playstation Blocking"
echo "Anti-Torrent Blocking"
echo " "
echo "Script by Teletubbins"
echo " "