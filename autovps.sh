#!/bin/bash
# =======================
# Thanks to Dreyannz
#
# Modified by Punish
# =======================

# Initializing Var
export DEBIAN_FRONTEND=noninteractive
OS=`uname -m`;
MYIP=$(wget -qO- ipv4.icanhazip.com);
MYIP2="s/xxxxxxxxx/$MYIP/g";


# Root Directory
cd

# Disable IPV6
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
sed -i '$ i\echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6' /etc/rc.local

# Install wget and curl
apt-get update;apt-get -y install wget curl;

# Local Time Manila
ln -fs /usr/share/zoneinfo/Asia/Manila /etc/localtime

# Local Configuration
sed -i 's/AcceptEnv/#AcceptEnv/g' /etc/ssh/sshd_config
service ssh restart

# Update
apt-get update

# Install Essential Packages
apt-get -y install nano iptables dnsutils openvpn screen whois ngrep unzip unrar

echo "clear"                                                                                      >> .bashrc
echo 'echo -e "\e[0m                                                                           "' >> .bashrc
echo 'echo -e "\e[94m      ____  ____  __    ____  ____  _  _  ____  ____  __  __ _  ____      "' >> .bashrc 
echo 'echo -e "\e[94m     (_  _)(  __)(  )  (  __)(_  _)/ )( \(  _ \(  _ \(  )(  ( \/ ___)     "' >> .bashrc
echo 'echo -e "\e[94m       )(   ) _) / (_/\ ) _)   )(  ) \/ ( ) _ ( ) _ ( )( /    /\___ \     "' >> .bashrc
echo 'echo -e "\e[94m      (__) (____)\____/(____) (__) \____/(____/(____/(__)\_)__)(____/     "' >> .bashrc
echo 'echo -e "\e[0m"'                                                                            >> .bashrc
echo 'echo -e "\e[94m                             Powered by Punish                            "' >> .bashrc                                                   
echo 'echo -e "\e[0m"'                                                                            >> .bashrc
echo 'echo -e "\e[94m                   Type menu to display list of commands                  "' >> .bashrc
echo 'echo -e "\e[0m                                                                           "' >> .bashrc

# Install WebServer
apt-get -y install nginx

# WebServer Configuration
cd
rm /etc/nginx/sites-enabled/default
rm /etc/nginx/sites-available/default
wget -O /etc/nginx/nginx.conf "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/nginx.conf"
mkdir -p /home/vps/public_html
echo "<h1><center>Powered by Punish</center></h1>" > /home/vps/public_html/index.html
wget -O /etc/nginx/conf.d/vps.conf "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/vps.conf"
service nginx restart

# Install OpenVPN
apt-get -y install openvpn easy-rsa openssl iptables
cp -r /usr/share/easy-rsa/ /etc/openvpn
mkdir /etc/openvpn/easy-rsa/keys
sed -i 's|export KEY_COUNTRY="US"|export KEY_COUNTRY="PH"|' /etc/openvpn/easy-rsa/vars
sed -i 's|export KEY_PROVINCE="CA"|export KEY_PROVINCE="Manila"|' /etc/openvpn/easy-rsa/vars
sed -i 's|export KEY_CITY="SanFrancisco"|export KEY_CITY="Manila"|' /etc/openvpn/easy-rsa/vars
sed -i 's|export KEY_ORG="Fort-Funston"|export KEY_ORG="IT"|' /etc/openvpn/easy-rsa/vars
sed -i 's|export KEY_EMAIL="me@myhost.mydomain"|export KEY_EMAIL="punisher@gmail.com"|' /etc/openvpn/easy-rsa/vars
sed -i 's|export KEY_OU="MyOrganizationalUnit"|export KEY_OU="IT"|' /etc/openvpn/easy-rsa/vars
sed -i 's|export KEY_NAME="EasyRSA"|export KEY_NAME="Xtremserver.com"|' /etc/openvpn/easy-rsa/vars
sed -i 's|export KEY_OU=changeme|export KEY_OU=Xtremserver.com|' /etc/openvpn/easy-rsa/vars

# Create Diffie-Helman Pem
openssl dhparam -out /etc/openvpn/dh2048.pem 2048

# Create PKI
cd /etc/openvpn/easy-rsa
. ./vars
./clean-all
export EASY_RSA="${EASY_RSA:-.}"
"$EASY_RSA/pkitool" --initca $*

# Create key server
export EASY_RSA="${EASY_RSA:-.}"
"$EASY_RSA/pkitool" --server server

# Setting KEY CN
export EASY_RSA="${EASY_RSA:-.}"
"$EASY_RSA/pkitool" client

# cp /etc/openvpn/easy-rsa/keys/{server.crt,server.key,ca.crt} /etc/openvpn
cd
cp /etc/openvpn/easy-rsa/keys/server.crt /etc/openvpn/server.crt
cp /etc/openvpn/easy-rsa/keys/server.key /etc/openvpn/server.key
cp /etc/openvpn/easy-rsa/keys/ca.crt /etc/openvpn/ca.crt

# Setting Server
cd /etc/openvpn/
wget "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/server.conf"

#Create OpenVPN Config
cd
mkdir -p /home/vps/public_html
cd /home/vps/public_html/
wget "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/client.ovpn"
sed -i $MYIP2 /home/vps/public_html/client.ovpn;
echo '<ca>' >> /home/vps/public_html/client.ovpn
cat /etc/openvpn/ca.crt >> /home/vps/public_html/client.ovpn
echo '</ca>' >> /home/vps/public_html/client.ovpn
cd /home/vps/public_html/
tar -czf /home/vps/public_html/openvpn.tar.gz client.ovpn
tar -czf /home/vps/public_html/client.tar.gz client.ovpn
cd

# Restart OpenVPN
/etc/init.d/openvpn restart
service openvpn start
service openvpn status

# Setting USW
apt-get install ufw
ufw allow ssh
ufw allow 1194/tcp
sed -i 's|DEFAULT_INPUT_POLICY="DROP"|DEFAULT_INPUT_POLICY="ACCEPT"|' /etc/default/ufw
sed -i 's|DEFAULT_FORWARD_POLICY="DROP"|DEFAULT_FORWARD_POLICY="ACCEPT"|' /etc/default/ufw
cd /etc/ufw/
iptables -t nat -I POSTROUTING -s 192.168.100.0/8 -o eth0 -j MASQUERADE
iptables-save > /etc/iptables_yg_baru_dibikin.conf
wget -O /etc/network/if-up.d/iptables "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/iptables-restore"
chmod +x /etc/network/if-up.d/iptables
cd
ufw enable
ufw status
ufw disable

# set ipv4 forward
echo 1 > /proc/sys/net/ipv4/ip_forward
sed -i 's|#net.ipv4.ip_forward=1|net.ipv4.ip_forward=1|' /etc/sysctl.conf

# Install BadVPN
cd
wget -O /usr/bin/badvpn-udpgw "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/badvpn-udpgw"
if [ "$OS" == "x86_64" ]; then
  wget -O /usr/bin/badvpn-udpgw "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/badvpn-udpgw64"
fi
sed -i '$ i\screen -AmdS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300' /etc/rc.local
chmod +x /usr/bin/badvpn-udpgw
screen -AmdS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300

# SSH Configuration
cd
sed -i 's/Port 22/Port 22/g' /etc/ssh/sshd_config
sed -i '/Port 22/a Port 143' /etc/ssh/sshd_config
service ssh restart

# Install Dropbear
apt-get -y install dropbear
sed -i 's/NO_START=1/NO_START=0/g' /etc/default/dropbear
sed -i 's/DROPBEAR_PORT=22/DROPBEAR_PORT=443/g' /etc/default/dropbear
sed -i 's/DROPBEAR_EXTRA_ARGS=/DROPBEAR_EXTRA_ARGS="-p 443 -p 80"/g' /etc/default/dropbear
echo "/bin/false" >> /etc/shells
echo "/usr/sbin/nologin" >> /etc/shells
service ssh restart
service dropbear restart

# Install Squid3
cd
apt-get -y install squid3
wget -O /etc/squid3/squid.conf "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/squid3.conf"
sed -i $MYIP2 /etc/squid3/squid.conf;
service squid3 restart

# Install WebMin
cd
apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python
echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
echo "deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib" >> /etc/apt/sources.list
wget http://www.webmin.com/jcameron-key.asc
apt-key add jcameron-key.asc
apt-get update
apt-get install webmin
sed -i 's/ssl=1/ssl=0/g' /etc/webmin/miniserv.conf
service webmin restart

# Install Stunnel
apt-get -y install stunnel4
wget -O /etc/stunnel/stunnel.pem "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/stunnel.pem"
wget -O /etc/stunnel/stunnel.conf "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/stunnel.conf"
sed -i $MYIP2 /etc/stunnel/stunnel.conf
sed -i 's/ENABLED=0/ENABLED=1/g' /etc/default/stunnel4
service stunnel4 restart

# Install Fail2Ban
apt-get -y install fail2ban;
service fail2ban restart

# Install DDOS Deflate
cd
apt-get -y install dnsutils dsniff
wget "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/ddos-deflate-master.zip"
unzip ddos-deflate-master.zip
cd ddos-deflate-master
./install.sh
cd
rm -rf ddos-deflate-master.zip

# Banner
rm /etc/issue.net
wget -O /etc/issue.net "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/issue.net"
sed -i 's@#Banner@Banner@g' /etc/ssh/sshd_config
sed -i 's@DROPBEAR_BANNER=""@DROPBEAR_BANNER="/etc/issue.net"@g' /etc/default/dropbear
service ssh restart
service dropbear restart

# XML Parser
cd
apt-get -y --force-yes -f install libxml-parser-perl

# Block Torrent
iptables -A OUTPUT -p tcp --dport 6881:6889 -j DROP
iptables -A OUTPUT -p udp --dport 1024:65534 -j DROP
iptables -A FORWARD -m string --string "get_peers" --algo bm -j DROP
iptables -A FORWARD -m string --string "announce_peer" --algo bm -j DROP
iptables -A FORWARD -m string --string "find_node" --algo bm -j DROP
iptables -A FORWARD -m string --algo bm --string "BitTorrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "BitTorrent protocol" -j DROP
iptables -A FORWARD -m string --algo bm --string "peer_id=" -j DROP
iptables -A FORWARD -m string --algo bm --string ".torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce.php?passkey=" -j DROP
iptables -A FORWARD -m string --algo bm --string "torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce" -j DROP
iptables -A FORWARD -m string --algo bm --string "info_hash" -j DROP

# Install Screenfetch
apt-get -y install lsb-release scrot
wget -O screenfetch "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/screenfetch"
chmod +x screenfetch

# download script
cd /usr/bin
wget -O menu "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/menu.sh"
wget -O create "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/create.sh"
wget -O trial "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/trial.sh"
wget -O remove "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/delete.sh"
wget -O check "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/check.sh"
wget -O member "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/member.sh"
wget -O banner "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/servermessage.sh"
wget -O restart "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/resvis.sh"
wget -O speedtest "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/speedtest"
wget -O info "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/info.sh"
wget -O about "https://bitbucket.org/teletubbins/xtrem/raw/cc53c8ae0269fba761256a9f5d27d1fa8a055431/about.sh"

# AutoReboot Tools
echo "0 0 * * * root /sbin/reboot" > /etc/cron.d/reboot

# Set Permissions
chmod +x menu
chmod +x create
chmod +x trial
chmod +x remove
chmod +x check
chmod +x member
chmod +x banner
chmod +x restart
chmod +x speedtest
chmod +x info
chmod +x about

# Finishing
cd
chown -R www-data:www-data /home/vps/public_html
service nginx start
service openvpn restart
service cron restart
service ssh restart
service dropbear restart
service stunnel4 restart
service squid3 restart
service webmin restart
rm -rf ~/.bash_history && history -c
echo "unset HISTFILE" >> /etc/profile

# grep ports 
opensshport="$(netstat -ntlp | grep -i ssh | grep -i 0.0.0.0 | awk '{print $4}' | cut -d: -f2)"
dropbearport="$(netstat -nlpt | grep -i dropbear | grep -i 0.0.0.0 | awk '{print $4}' | cut -d: -f2)"
stunnel4port="$(netstat -nlpt | grep -i stunnel | grep -i 0.0.0.0 | awk '{print $4}' | cut -d: -f2)"
openvpnport="$(netstat -nlpt | grep -i openvpn | grep -i 0.0.0.0 | awk '{print $4}' | cut -d: -f2)"
squidport="$(cat /etc/squid3/squid.conf | grep -i http_port | awk '{print $2}')"
nginxport="$(netstat -nlpt | grep -i nginx| grep -i 0.0.0.0 | awk '{print $4}' | cut -d: -f2)"

# Info
clear
echo -e ""
echo -e "\e[94m##################################################"
echo -e "\e[0m                                                   "
echo -e "\e[94m           AutoScript Powered by Punish           "
echo -e "\e[94m                                                  "
echo -e "\e[94m    Host/IP        :   $MYIP                      "
echo -e "\e[94m    OpenSSH        :   22, 143                    "
echo -e "\e[94m    Dropbear       :   80, 443                    "
echo -e "\e[94m    Squid Port     :   8080, 3128                 "
echo -e "\e[94m    SSL            :   442                        "
echo -e "\e[94m    OpenVPN        :   TCP 1194                   "
echo -e "\e[94m    BadVPN         :   UDPGW 7300                 "
echo -e "\e[94m    Nginx          :   81                         "
echo -e "\e[94m                                                  "
echo -e "\e[94m             Other Features Included              "
echo -e "\e[94m                                                  "
echo -e "\e[94m    Timezone       :   Asia/Manila (GMT +8)       "
echo -e "\e[94m    Webmin         :   http://$MYIP:10000/        "
echo -e "\e[94m    IPV6           :   [OFF]                      "
echo -e "\e[94m    Cron Scheduler :   [ON]                       "
echo -e "\e[94m    Fail2Ban       :   [ON]                       "
echo -e "\e[94m    DDOS Deflate   :   [ON]                       "
echo -e "\e[94m    LibXML Parser  :   {ON]                       "
echo -e "\e[0m                                                   "
echo -e "\e[94m##################################################"
echo -e "\e[0m                                                   "
read -n1 -r -p "         Press Any Key To Show Commands          "
menu
cd
rm -f /root/debian8.sh